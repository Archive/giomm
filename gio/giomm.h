#ifndef _GIOMM_H
#define _GIOMM_H

/* 
 * Copyright (C) 2007 The giomm Development Team
 * 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <giomm/asyncresult.h>
#include <giomm/cancellable.h>
#include <giomm/drive.h>
#include <giomm/fileattribute.h>
#include <giomm/file.h>
#include <giomm/fileicon.h>
#include <giomm/fileinfo.h>
#include <giomm/fileinputstream.h>
#include <giomm/fileoutputstream.h>
#include <giomm/icon>
#include <giomm/init.h>
#include <giomm/inputstream.h>
#include <giomm/mountoperation.h>
#include <giomm/outputstream.h>
#include <giomm/simpleasyncresult.h>

#endif /* #ifndef _GIOMM_H */

