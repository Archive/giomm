// -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 2 -*-

/* Copyright (C) 2007 The giomm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gio/goutputstream.h>

#include <glibmm/object.h>
#include <giomm/asyncresult.h>
#include <giomm/cancellable.h>
#include <giomm/inputstream.h>

_DEFS(giomm,gio)
_PINCLUDE(glibmm/private/object_p.h)

namespace Gio {

_WRAP_ENUM(OutputStreamSpliceFlags, GOutputStreamSpliceFlags, NO_GTYPE)

class OutputStream : public Glib::Object
{
  _CLASS_GOBJECT(OutputStream, GOutputStream, G_OUTPUT_STREAM, Glib::Object, GObject)

public:
  _WRAP_METHOD(gssize write(const void* buffer, gsize count, const Glib::RefPtr<Cancellable>& cancellable),
               g_output_stream_write,
               errthrow)

  _WRAP_METHOD(bool write_all(const void* buffer,
                              gsize count,
                              gsize& bytes_written,
                              const Glib::RefPtr<Cancellable>& cancellable),
               g_output_stream_write_all,
               errthrow)

  _WRAP_METHOD(gssize splice(const Glib::RefPtr<InputStream>& source,
                             OutputStreamSpliceFlags flags,
                             const Glib::RefPtr<Cancellable>& cancellable),
               g_output_stream_splice,
               errthrow)

  _WRAP_METHOD(bool flush(const Glib::RefPtr<Cancellable>& cancellable),
               g_output_stream_flush,
               errthrow)

  _WRAP_METHOD(bool close(const Glib::RefPtr<Cancellable>& cancellable),
               g_output_stream_close,
               errthrow)

  _IGNORE(g_output_stream_write_async)
  void write_async(const void* buffer,
                   gsize count,
                   int io_priority,
                   const Glib::RefPtr<Cancellable>& cancellable,
                   const SlotAsyncReady& slot);

  _WRAP_METHOD(gssize write_finish(Glib::RefPtr<AsyncResult>& result),
               g_output_stream_write_finish,
               errthrow)

  _IGNORE(g_output_stream_splice_async)
  void splice_async(const Glib::RefPtr<InputStream>& source,
                    OutputStreamSpliceFlags flags,
                    int io_priority,
                    const Glib::RefPtr<Cancellable>& cancellable,
                    const SlotAsyncReady& slot);

  _WRAP_METHOD(gssize splice_finish(Glib::RefPtr<AsyncResult>& result),
               g_output_stream_splice_finish,
               errthrow)

  _IGNORE(g_output_stream_flush_async)
  void flush_async(int io_priority,
                   const Glib::RefPtr<Cancellable>& cancellable,
                   const SlotAsyncReady& slot);

  _WRAP_METHOD(bool flush_finish(Glib::RefPtr<AsyncResult>& result),
               g_output_stream_flush_finish,
               errthrow)

  _IGNORE(g_output_stream_close_async)
  void close_async(int io_priority,
                   const Glib::RefPtr<Cancellable>& cancellable,
                   const SlotAsyncReady& slot);

  _WRAP_METHOD(bool close_finish(Glib::RefPtr<AsyncResult>& result),
               g_output_stream_close_finish,
               errthrow)

  _WRAP_METHOD(bool is_closed() const, g_output_stream_is_closed)

  _WRAP_METHOD(bool has_pending() const, g_output_stream_has_pending)

  _WRAP_METHOD(void set_pending(bool pending = true), g_output_stream_set_pending)

  //TODO: vfuncs and overloads without Cancellable
};

} // namespace Gio
