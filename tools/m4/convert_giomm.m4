
# enum conversions
_CONV_ENUM(G,PasswordSave)
_CONV_ENUM(G,FileAttributeType)
_CONV_ENUM(G,FileAttributeFlags)
_CONV_ENUM(G,FileCopyFlags)
_CONV_ENUM(G,FileCreateFlags)
_CONV_ENUM(G,FileMonitorFlags)
_CONV_ENUM(G,FileQueryInfoFlags)
_CONV_ENUM(G,FileType)
_CONV_ENUM(G,OutputStreamSpliceFlags)

# strings (can we import glibmm convert file?)
#_CONVERSION(`const char*',`std::string',__GCHARP_TO_STDSTRING)
_CONVERSION(`const char*',`const std::string&',__GCHARP_TO_STDSTRING)
_CONVERSION(`char*',`std::string',__GCHARP_TO_STDSTRING)
_CONVERSION(`std::string', `char*', `g_strdup(($3).c_str())')
_CONVERSION(`const std::string&', `char*', `g_strdup(($3).c_str())')
_CONVERSION(`Glib::ustring', `char*', `g_strdup(($3).c_str())')

# AsyncResult
_CONVERSION(`Glib::RefPtr<Glib::Object>',`GObject*',__CONVERT_REFPTR_TO_P)
_CONVERSION(`const Glib::RefPtr<AsyncResult>&',`GAsyncResult*',__CONVERT_REFPTR_TO_P)
_CONVERSION(`Glib::RefPtr<AsyncResult>&',`GAsyncResult*',__CONVERT_REFPTR_TO_P)

# Cancellable
_CONVERSION(`const Glib::RefPtr<Cancellable>&',`GCancellable*',__CONVERT_CONST_REFPTR_TO_P)

# Drive
_CONVERSION(`GDrive*',`Glib::RefPtr<Drive>',`Glib::wrap($3)')

# File
_CONVERSION(`return-char*',`std::string',`Glib::convert_return_gchar_ptr_to_stdstring($3)')
_CONVERSION(`Glib::RefPtr<File>',`GFile*',__CONVERT_REFPTR_TO_P)
_CONVERSION(`const Glib::RefPtr<File>&',`GFile*',__CONVERT_CONST_REFPTR_TO_P)
_CONVERSION(`GFile*',`Glib::RefPtr<File>',`Glib::wrap($3)')
_CONVERSION(`GFile*',`const Glib::RefPtr<File>&',`Glib::wrap($3, true)')

# FileAttribute
_CONVERSION(`GFileAttributeValue*',`FileAttributeValue',`Glib::wrap($3)')
_CONVERSION(`const FileAttributeValue&',`const GFileAttributeValue*',`$3.gobj()')
_CONVERSION(`GFileAttributeInfoList*',`Glib::RefPtr<FileAttributeInfoList>',`Glib::wrap($3)')

#FileEnumerator
_CONVERSION(`GFileEnumerator*',`Glib::RefPtr<FileEnumerator>',`Glib::wrap($3)')

# FileInfo
_CONVERSION(`GFileInfo*',`Glib::RefPtr<FileInfo>',`Glib::wrap($3)')
_CONVERSION(`Glib::RefPtr<FileInfo>&',`GFileInfo*',__CONVERT_REFPTR_TO_P)
_CONVERSION(`const Glib::RefPtr<FileInfo>&',`GFileInfo*',__CONVERT_REFPTR_TO_P)
_CONVERSION(`char**',`Glib::StringArrayHandle',`Glib::StringArrayHandle($3)')
_CONVERSION(`Glib::TimeVal&', `GTimeVal*', static_cast<$2>(&$3))
_CONVERSION(`const Glib::RefPtr<FileAttributeMatcher>&',`GFileAttributeMatcher*',__CONVERT_CONST_REFPTR_TO_P)
_CONVERSION(`GList*',`Glib::ListHandle< Glib::RefPtr<FileInfo> >',__FL2H_SHALLOW)

# FileInputStream
_CONVERSION(`GFileInputStream*',`Glib::RefPtr<FileInputStream>',`Glib::wrap($3)')

# FileOutputStream
_CONVERSION(`GFileOutputStream*',`Glib::RefPtr<FileOutputStream>',`Glib::wrap($3)')

# Icon
_CONVERSION(`GIcon*',`Glib::RefPtr<Icon>',`Glib::wrap($3)')
_CONVERSION(`const Glib::RefPtr<Icon>&',`GIcon*',__CONVERT_CONST_REFPTR_TO_P)

# InputStream
_CONVERSION(`const Glib::RefPtr<InputStream>&',`GInputStream*',__CONVERT_CONST_REFPTR_TO_P)

# MountOptions
_CONVERSION(`GPasswordSave',`PasswordSave',`($2)$3')
_CONVERSION(`PasswordSave',`GPasswordSave',`($2)$3')

#Volume
_CONVERSION(`GVolume*',`Glib::RefPtr<Volume>',`Glib::wrap($3)')

#SomeClass
_CONVERSION(`GSList*',`Glib::SListHandle<Glib::ustring>', `$2($3, Glib::OWNERSHIP_DEEP)')
_CONVERSION(`GList*',`Glib::ListHandle<Glib::ustring>', `$2($3, Glib::OWNERSHIP_DEEP)')
_CONVERSION(`const Glib::SListHandle<const Glib::ustring>&',`const GSList*', `$3.data()')
_CONVERSION(`Glib::ListHandle<const Glib::ustring>&',`const GSList*', `$3.data()')
_CONVERSION(`Glib::ListHandle<Glib::ustring>',`GSList*', `$3.data()')
